package ru.t1.shevyreva.tm.exception.system;

public final class RequestEmptyException extends AbstractSystemException {

    public RequestEmptyException() {
        super("Error! Request is empty ! ");
    }

}
