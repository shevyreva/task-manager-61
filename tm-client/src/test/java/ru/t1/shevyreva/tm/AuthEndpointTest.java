package ru.t1.shevyreva.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserProfileRequest;
import ru.t1.shevyreva.tm.dto.response.user.UserLoginResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserProfileResponse;
import ru.t1.shevyreva.tm.marker.SoapCategory;


@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    public void login() {
        @NotNull final UserLoginRequest testRequest = new UserLoginRequest("test", "test");
        @NotNull final UserLoginResponse testResponse = authEndpoint.login(testRequest);
        Assert.assertNotNull(testResponse);
        Assert.assertNotNull(testResponse.getToken());

        @NotNull final UserLoginRequest adminRequest = new UserLoginRequest("admin", "admin");
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(adminRequest);
        Assert.assertNotNull(adminResponse);
        Assert.assertNotNull(adminResponse.getToken());

        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(null));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(null, "test")));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest("test", null)));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest("incorrect", "incorrect")));

        authEndpoint.logout(new UserLogoutRequest(testResponse.getToken()));
        authEndpoint.logout(new UserLogoutRequest(adminResponse.getToken()));
    }

    @Test
    public void profile() {
        @NotNull final UserLoginRequest testRequest = new UserLoginRequest("test", "test");
        @NotNull final UserLoginResponse testResponse = authEndpoint.login(testRequest);
        Assert.assertNotNull(testResponse);
        @NotNull final String testToken = testResponse.getToken();

        @NotNull final UserProfileRequest request = new UserProfileRequest(testToken);
        @NotNull final UserProfileResponse response = authEndpoint.profile(request);

        Assert.assertNotNull(response.getUser().getLogin());
        Assert.assertNotNull(response.getUser().getPasswordHash());
        Assert.assertEquals("test", response.getUser().getLogin());

        Assert.assertThrows(Exception.class, () ->
                authEndpoint.profile(null));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.profile(new UserProfileRequest(null)));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.profile(new UserProfileRequest("incorrect")));
    }

    @Test
    public void logout() {
        @NotNull final UserLoginRequest testRequest = new UserLoginRequest("test", "test");
        @NotNull final UserLoginResponse testResponse = authEndpoint.login(testRequest);
        Assert.assertNotNull(testResponse);
        @NotNull final String testToken = testResponse.getToken();

        Assert.assertThrows(Exception.class, () ->
                authEndpoint.logout(null));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.logout(new UserLogoutRequest(null)));
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.logout(new UserLogoutRequest("incorrect")));

        @NotNull final UserLogoutResponse response = authEndpoint.logout(new UserLogoutRequest(testToken));
        Assert.assertNotNull(response);
        Assert.assertNull(response.getUser());
    }

}
