package ru.t1.shevyreva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class TaskStartStatusByIdListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Task start by Id.";

    @NotNull
    private final String NAME = "task-start-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskStartStatusByIdListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, Status.IN_PROGRESS);
        taskEndpoint.changeTaskStatusById(request);
    }

}
