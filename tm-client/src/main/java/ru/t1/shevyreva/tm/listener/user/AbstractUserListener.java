package ru.t1.shevyreva.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shevyreva.tm.api.endpoint.IUserEndpoint;
import ru.t1.shevyreva.tm.listener.AbstractListener;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    IUserEndpoint userEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Id: " + user.getId());
    }

}
