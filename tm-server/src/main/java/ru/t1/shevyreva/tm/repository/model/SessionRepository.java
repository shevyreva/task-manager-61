package ru.t1.shevyreva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.model.Session;

import java.util.List;
import java.util.Optional;

@Repository
public interface SessionRepository extends AbstractUserOwnedModelRepository<Session> {

    long count();

    @Transactional
    void deleteAll();

    @Override
    @Transactional
    void deleteById(@NotNull final String id);

    @Transactional
    void deleteByUserId(@NotNull final String userId);

    @NotNull
    List<Session> findAll();

    @NotNull
    @Override
    Optional<Session> findById(@NotNull final String id);

    @NotNull
    Session findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
