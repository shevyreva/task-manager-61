package ru.t1.shevyreva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.shevyreva.tm.model.User;

import java.util.List;

@Repository
public interface UserRepository extends AbstractModelRepository<User> {

    @Override
    @Transactional
    void deleteAll();

    @Override
    @Nullable List<User> findAll();

    @Nullable User findOneById(@NotNull String id);

    @Transactional
    void deleteOneById(@NotNull String id);

    @Override
    long count();

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User findByEmail(@NotNull final String email);

    @Override
    @Transactional
    void delete(@NotNull final User user);

}
